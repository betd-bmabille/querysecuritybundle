# QuerySecurityBundle

WIP : Bundle

## Instalation 

 Composer require 
 
 
## Configuration 


```
parameters:
  # All Fields protection
  
  html_escaped: true # all field are passed to strip_tags function
  denied: ['<script>','javascript', 'http', '//']  # banned word in field value
  escaped: ['redirect']  ## word removed from value

  # per query parameters configuration
  parameters:
     fieldname:
        denied: ['<script>','javascript', 'http', '//']
        
        #can access submit field if field is array
        subfield:
            html_escaped: false
 ```
 
 
 ## Todo 
 
 [ ] Better Configuration