<?php

namespace BusinessDecision\Bundle\QuerySecurityBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class QuerySecuritySubscriber.
 */
class QuerySecuritySubscriber
{
    /** @var ContainerBuilder */
    private $container;

    /** @var array */
    private $mapping = [
        'html_escaped' => 'stripTags',
        'denied' => 'denied',
        'escaped' => 'escaped',
    ];

    /** @var string */
    private $actualParam = '';

    /**
     * QuerySecuritySubscriber constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        foreach ($event->getRequest()->query as $param => $value) {
            $this->checkParam($param, $value);
            $event->getRequest()->query->set($param, $value);
        }
    }

    /**
     * @param string $value
     * @param bool   $securityConf
     *
     * @return string
     */
    public function stripTags(string $value, bool $securityConf): string
    {
        if (empty($securityConf)) {
            return $value;
        }

        return strip_tags($value);
    }

    /**
     * @param string $value
     * @param array  $securityConf
     *
     * @return string
     */
    public function escaped(string $value, array $securityConf = []): string
    {
        if (!empty($securityConf)) {
            return str_replace($securityConf, '', $value);
        }

        return $value;
    }

    /**
     * @param string $value
     * @param array  $securityConf
     *
     * @throws AccessDeniedHttpException
     *
     * @return string
     */
    public function denied(string $value, array $securityConf = []): string
    {
        if (!empty($securityConf)) {
            foreach ($securityConf as $denied) {
                if (false !== strpos($value, $denied)) {
                    throw new AccessDeniedHttpException(
                        sprintf(
                            'This terms [%s] are denied in %s parameter',
                            implode(', ', $securityConf),
                            $this->actualParam
                        )
                    );
                }
            }
        }

        return $value;
    }

    /**
     * @param string $param
     * @param mixed  $value
     *
     * @return array|string
     */
    private function checkParam(string $param, $value)
    {
        if (\is_string($value)) {
            $this->actualParam = $param;

            return $this->checkStringParam($param, $value);
        }
        if (\is_array($value)) {
            foreach ($value as $subParam => $subValue) {
                $value[$subParam] = $this->checkParam($param.'.'.$subParam, $subValue);
            }
        }

        return $value;
    }

    /**
     * @param string $param
     * @param string $value
     *
     * @return string
     */
    private function checkStringParam(string $param, string $value)
    {
        $value = $this->checkSecurityValidity('denied', $param, $value);
        $value = $this->checkSecurityValidity('escaped', $param, $value);
        $value = $this->checkSecurityValidity('html_escaped', $param, $value);

        return $value;
    }

    /**
     * @param string $type
     * @param string $param
     * @param string $value
     *
     * @return string
     */
    private function checkSecurityValidity(string $type, string $param, string $value): string
    {
        $securityConf = $this->getSecurityConfiguration($type, $param);
        if (empty($securityConf)) {
            return $value;
        }

        return $this->process($value, $securityConf, $this->mapping[$type]);
    }

    /**
     * @param string $value
     * @param mixed  $securityConf
     * @param string $callback
     *
     * @return string
     */
    private function process(string $value, $securityConf, string $callback = ''): string
    {
        if (!empty($callback)) {
            return $this->$callback($value, $securityConf);
        }

        return $value;
    }

    /**
     * @param string $type
     * @param string $param
     *
     * @return bool|mixed
     */
    private function getSecurityConfiguration(string $type, string $param = '')
    {
        try {
            return $this->container->getParameter('parameters.'.$param.'.'.$type);
        } catch (\InvalidArgumentException $e) {
            try {
                return $this->container->getParameter($type);
            } catch (\InvalidArgumentException $e) {
                return false;
            }
        }
    }
}
